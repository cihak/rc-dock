import React from "react";
import { DockContextType } from "./DockData";
import { DockTabs } from "./DockTabs";
import { DragDropDiv } from "./dragdrop/DragDropDiv";
import { DragState } from "./dragdrop/DragManager";
import { DockDropLayer } from "./DockDropLayer";
import { getFloatPanelSize, nextZIndex } from "./Algorithm";
import { DockDropEdge } from "./DockDropEdge";
export class DockPanel extends React.PureComponent {
    constructor() {
        super(...arguments);
        this.getRef = (r) => {
            this._ref = r;
        };
        this.state = { dropFromPanel: null, draggingHeader: false };
        this.onDragOver = (e) => {
            if (DockPanel._droppingPanel === this) {
                return;
            }
            let { panelData } = this.props;
            let dockId = this.context.getDockId();
            let tab = DragState.getData('tab', dockId);
            let panel = DragState.getData('panel', dockId);
            if (tab || panel) {
                DockPanel.droppingPanel = this;
            }
            if (tab) {
                if (tab.parent) {
                    this.setState({ dropFromPanel: tab.parent });
                }
                else {
                    // add a fake panel
                    this.setState({ dropFromPanel: { activeId: '', tabs: [], group: tab.group } });
                }
            }
            else if (panel) {
                this.setState({ dropFromPanel: panel });
            }
        };
        // drop to move in float mode
        this.onPanelHeaderDragStart = (event) => {
            let { panelData } = this.props;
            let { parent, x, y, z } = panelData;
            let dockId = this.context.getDockId();
            if (parent && parent.mode === 'float') {
                this._movingX = x;
                this._movingY = y;
                // hide the panel, but not create drag layer element
                event.setData({ panel: this.props.panelData }, dockId);
                event.startDrag(null, null);
                this.onFloatPointerDown();
            }
            else {
                let tabGroup = this.context.getGroup(panelData.group);
                let [panelWidth, panelHeight] = getFloatPanelSize(this._ref, tabGroup);
                event.setData({ panel: panelData, panelSize: [panelWidth, panelHeight] }, dockId);
                event.startDrag(null);
            }
            this.setState({ draggingHeader: true });
        };
        this.onPanelHeaderDragMove = (e) => {
            let { width, height } = this.context.getLayoutSize();
            let { panelData } = this.props;
            panelData.x = this._movingX + e.dx;
            panelData.y = this._movingY + e.dy;
            if (width > 200 && height > 200) {
                if (panelData.y < 0) {
                    panelData.y = 0;
                }
                else if (panelData.y > height - 16) {
                    panelData.y = height - 16;
                }
                if (panelData.x + panelData.w < 16) {
                    panelData.x = 16 - panelData.w;
                }
                else if (panelData.x > width - 16) {
                    panelData.x = width - 16;
                }
            }
            this.forceUpdate();
        };
        this.onPanelHeaderDragEnd = (e) => {
            if (!this._unmounted) {
                this.setState({ draggingHeader: false });
                this.context.onSilentChange(this.props.panelData.activeId, 'move');
            }
        };
        this.onPanelCornerDragTL = (e) => {
            this.onPanelCornerDrag(e, 'tl');
        };
        this.onPanelCornerDragTR = (e) => {
            this.onPanelCornerDrag(e, 'tr');
        };
        this.onPanelCornerDragBL = (e) => {
            this.onPanelCornerDrag(e, 'bl');
        };
        this.onPanelCornerDragBR = (e) => {
            this.onPanelCornerDrag(e, 'br');
        };
        this.onPanelCornerDragMove = (e) => {
            let { panelData } = this.props;
            let { dx, dy } = e;
            if (this._movingCorner.startsWith('t')) {
                // when moving top corners, dont let it move header out of screen
                let { width, height } = this.context.getLayoutSize();
                if (this._movingY + dy < 0) {
                    dy = -this._movingY;
                }
                else if (this._movingY + dy > height - 16) {
                    dy = height - 16 - this._movingY;
                }
            }
            switch (this._movingCorner) {
                case 'tl': {
                    panelData.x = this._movingX + dx;
                    panelData.w = this._movingW - dx;
                    panelData.y = this._movingY + dy;
                    panelData.h = this._movingH - dy;
                    break;
                }
                case 'tr': {
                    panelData.w = this._movingW + dx;
                    panelData.y = this._movingY + dy;
                    panelData.h = this._movingH - dy;
                    break;
                }
                case 'bl': {
                    panelData.x = this._movingX + dx;
                    panelData.w = this._movingW - dx;
                    panelData.h = this._movingH + dy;
                    break;
                }
                case 'br': {
                    panelData.w = this._movingW + dx;
                    panelData.h = this._movingH + dy;
                    break;
                }
            }
            this.forceUpdate();
        };
        this.onPanelCornerDragEnd = (e) => {
            this.context.onSilentChange(this.props.panelData.activeId, 'move');
        };
        this.onFloatPointerDown = () => {
            const isPanelEmpty = !Boolean(this.props.panelData.tabs.length);
            if (!isPanelEmpty) { //we dont want to increment z-index of an empty panel
                const z = 'z' in this.props.panelData && this.props.panelData.z ? this.props.panelData.z : 0;
                const newZ = nextZIndex(z);
                if (newZ !== z) {
                    this.props.panelData.z = newZ;
                    this.forceUpdate();
                }
            }
        };
        this.onPanelClicked = () => {
            if (!this._ref.contains(this._ref.ownerDocument.activeElement)) {
                this._ref.querySelector('.dock-bar').focus();
            }
        };
        this._resizer = null;
        this.dockboxPanelResizerSize = 6; //constant
        this.dockboxPanelResizerOffset = 3; //constant
        this.topOffsetToSubtract = 133; //variable
        //TODO
        this.handleMouseEnter = (direction) => (e) => {
            this.topOffsetToSubtract = document.querySelector(".dock-layout").offsetTop;
            let dividers = Array.from(document.querySelectorAll(".dock-divider"));
            dividers.forEach(divider => divider.removeAttribute('style'));
            // dividers.forEach(divider => console.log(divider.getBoundingClientRect()));
            const { pageX, pageY } = e;
            // console.log({e:e});
            // console.log({pageX: pageX, pageY: pageY});
            // console.log({direction: direction});
            // console.log({dividers: dividers});
            // console.log({'e.target':e.target});
            if (direction === 'right') {
                const dividerToUse = dividers.find(divider => (pageY >= divider.getBoundingClientRect().top && pageY <= divider.getBoundingClientRect().top + divider.getBoundingClientRect().height) && -1 < (divider.getBoundingClientRect().x - pageX) && (divider.getBoundingClientRect().x - pageX) < 20);
                if (dividerToUse) {
                    if (this._ref) {
                        const refRect = this._ref.getBoundingClientRect();
                        this._resizer = dividerToUse;
                        this._resizer.setAttribute('style', `background: transparent;
              position: absolute;
              width: ${this.dockboxPanelResizerSize}px;
              height: ${refRect.height}px;
              left: ${refRect.right - this.dockboxPanelResizerSize + this.dockboxPanelResizerOffset}px;
              top: ${refRect.top - this.topOffsetToSubtract}px;
              z-index: ${this.props.panelData.z};
              `);
                    }
                }
            }
            if (direction === 'left') {
                const dividerToUse = dividers.find(divider => (pageY >= divider.getBoundingClientRect().top && pageY <= divider.getBoundingClientRect().top + divider.getBoundingClientRect().height) && divider.getBoundingClientRect().width === 0 && -1 < (pageX - divider.getBoundingClientRect().x) && (pageX - divider.getBoundingClientRect().x) < 20);
                if (dividerToUse) {
                    if (this._ref) {
                        const refRect = this._ref.getBoundingClientRect();
                        this._resizer = dividerToUse;
                        this._resizer.setAttribute('style', `background: transparent;
              position: absolute;
              width: ${this.dockboxPanelResizerSize}px;
              height: ${refRect.height}px;
              left: ${refRect.left - this.dockboxPanelResizerSize + this.dockboxPanelResizerOffset}px;
              top: ${refRect.top - this.topOffsetToSubtract}px;
              z-index: ${this.props.panelData.z + 1};
              `);
                    }
                }
            }
            if (direction === 'top') {
                const dividerToUse = dividers.find(divider => (pageX >= divider.getBoundingClientRect().left && pageX <= divider.getBoundingClientRect().left + divider.getBoundingClientRect().width) && divider.getBoundingClientRect().height === 0 && -1 < (pageY - divider.getBoundingClientRect().y) && (pageY - divider.getBoundingClientRect().y) < 20);
                if (dividerToUse) {
                    if (this._ref) {
                        const refRect = this._ref.getBoundingClientRect();
                        this._resizer = dividerToUse;
                        this._resizer.setAttribute('style', `background: transparent;
              position: absolute;
              width: ${refRect.width}px;
              height: ${this.dockboxPanelResizerSize}px;
              left: ${refRect.left}px;
              top: ${refRect.top - this.topOffsetToSubtract - this.dockboxPanelResizerOffset}px;
              z-index: ${this.props.panelData.z + 1};
              `);
                    }
                }
            }
            if (direction === 'bottom') {
                const dividerToUse = dividers.find(divider => (pageX >= divider.getBoundingClientRect().left && pageX <= divider.getBoundingClientRect().left + divider.getBoundingClientRect().width) && -1 < (divider.getBoundingClientRect().y - pageY) && (divider.getBoundingClientRect().y - pageY) < 20);
                if (dividerToUse) {
                    if (this._ref) {
                        const refRect = this._ref.getBoundingClientRect();
                        this._resizer = dividerToUse;
                        this._resizer.setAttribute('style', `background: transparent;
              position: absolute;
              width: ${refRect.width}px;
              height: ${this.dockboxPanelResizerSize}px;
              left: ${refRect.left}px;
              top: ${refRect.bottom - this.topOffsetToSubtract - this.dockboxPanelResizerOffset}px;
              z-index: ${this.props.panelData.z + 1};
              `);
                    }
                }
            }
        };
        this.handleMouseLeave = (e) => {
            if (this._resizer && this._resizer !== document.elementFromPoint(e.pageX, e.pageY)) {
                this._resizer.removeAttribute('style');
                this._resizer = null;
            }
        };
        this._unmounted = false;
    }
    static set droppingPanel(panel) {
        if (DockPanel._droppingPanel === panel) {
            return;
        }
        if (DockPanel._droppingPanel) {
            DockPanel._droppingPanel.onDragOverOtherPanel();
        }
        DockPanel._droppingPanel = panel;
    }
    onDragOverOtherPanel() {
        if (this.state.dropFromPanel) {
            this.setState({ dropFromPanel: null });
        }
    }
    onPanelCornerDrag(e, corner) {
        let { parent, x, y, w, h } = this.props.panelData;
        if (parent && parent.mode === 'float') {
            this._movingCorner = corner;
            this._movingX = x;
            this._movingY = y;
            this._movingW = w;
            this._movingH = h;
            e.startDrag(null, null);
        }
    }
    render() {
        let { dropFromPanel, draggingHeader } = this.state;
        let { panelData, size, canDockedPanelsBeMovedToFront } = this.props;
        let { minWidth, minHeight, group: styleName, id, parent, panelLock } = panelData;
        if (panelLock) {
            if (panelLock.panelStyle) {
                styleName = panelLock.panelStyle;
            }
        }
        let panelClass;
        if (styleName) {
            if (styleName === 'tabGroup-placeholder' && panelData.size < 10) {
                setTimeout(() => {
                    this.context.dockMove(panelData, null, 'remove');
                }, 0);
                this.componentWillUnmount();
                return null;
            }
            panelClass = styleName
                .split(' ')
                .map((name) => `dock-style-${name}`)
                .join(' ');
        }
        const isEmpty = !Boolean(panelData.tabs.length);
        let isMax = parent && parent.mode === 'maximize';
        let isFloat = parent && parent.mode === 'float';
        let pointerDownCallback = this.onFloatPointerDown;
        let onPanelHeaderDragStart = this.onPanelHeaderDragStart;
        if (isMax || (!isFloat && !canDockedPanelsBeMovedToFront)) {
            pointerDownCallback = null;
        }
        if (isMax) {
            dropFromPanel = null;
            onPanelHeaderDragStart = null;
        }
        let cls = `dock-panel ${panelClass ? panelClass : ''}${dropFromPanel ? ' dock-panel-dropping' : ''}${draggingHeader ? ' dragging' : ''}`;
        let style = { minWidth, minHeight, flex: `${size} 1 ${size}px` };
        if (isFloat) {
            style.left = panelData.x;
            style.top = panelData.y;
            style.width = panelData.w;
            style.height = panelData.h;
            style.zIndex = panelData.z;
        }
        if (isEmpty) { //if the panel is empty, it will be rendered with zindex zero
            style.zIndex = 0;
        }
        else { // if the panel is not empty, and has a specified zindex, we use it
            if ('z' in panelData && panelData.z !== null && panelData.z !== undefined) {
                style.zIndex = panelData.z;
            }
            else { //if the panel is not empty, but has no specified zindex, set zero
                style.zIndex = 0;
            }
        }
        if ('alwaysOnTop' in panelData && panelData.alwaysOnTop) {
            if ('z' in panelData && panelData.z !== null && panelData.z !== undefined) {
                style.zIndex = panelData.z + 1000;
            }
            // panel.z = nextZIndex()+100;
        }
        let droppingLayer;
        // console.log('about to create droppingLayer');
        if (dropFromPanel) {
            let tabGroup = this.context.getGroup(dropFromPanel.group);
            let dockId = this.context.getDockId();
            // dont allow locked tab to create new panel
            if (!tabGroup.tabLocked || DragState.getData('tab', dockId) == null) {
                let DockDropClass = this.context.useEdgeDrop() ? DockDropEdge : DockDropLayer;
                if (this._ref) {
                    const tabsHeight = this._ref.children[0].children[0].getBoundingClientRect().height;
                    const { width, height, left, top } = this._ref.children[0].children[1].getBoundingClientRect();
                    const topOffset = this._ref.offsetTop;
                    const dropLayerStyle = {
                        width: width,
                        height: height,
                        left: left,
                        top: topOffset + tabsHeight,
                        zIndex: style.zIndex
                    };
                    droppingLayer = React.createElement(DockDropClass, { style: dropLayerStyle, panelData: panelData, panelElement: this._ref, dropFromPanel: dropFromPanel });
                }
            }
            else {
            }
        }
        else {
        }
        return (React.createElement(React.Fragment, null,
            React.createElement(DragDropDiv, { getRef: this.getRef, className: cls, style: style, "data-dockid": id, onMouseDownCapture: pointerDownCallback, onTouchStartCapture: pointerDownCallback, onDragOverT: isFloat ? null : this.onDragOver, onClick: this.onPanelClicked },
                React.createElement(DockTabs, { panelData: panelData, onPanelDragStart: onPanelHeaderDragStart, onPanelDragMove: this.onPanelHeaderDragMove, onPanelDragEnd: this.onPanelHeaderDragEnd }),
                isFloat ?
                    [
                        React.createElement(DragDropDiv, { key: "drag-size-t-l", className: "dock-panel-drag-size dock-panel-drag-size-t-l", onDragStartT: this.onPanelCornerDragTL, onDragMoveT: this.onPanelCornerDragMove, onDragEndT: this.onPanelCornerDragEnd }),
                        React.createElement(DragDropDiv, { key: "drag-size-t-r", className: "dock-panel-drag-size dock-panel-drag-size-t-r", onDragStartT: this.onPanelCornerDragTR, onDragMoveT: this.onPanelCornerDragMove, onDragEndT: this.onPanelCornerDragEnd }),
                        React.createElement(DragDropDiv, { key: "drag-size-b-l", className: "dock-panel-drag-size dock-panel-drag-size-b-l", onDragStartT: this.onPanelCornerDragBL, onDragMoveT: this.onPanelCornerDragMove, onDragEndT: this.onPanelCornerDragEnd }),
                        React.createElement(DragDropDiv, { key: "drag-size-b-r", className: "dock-panel-drag-size dock-panel-drag-size-b-r", onDragStartT: this.onPanelCornerDragBR, onDragMoveT: this.onPanelCornerDragMove, onDragEndT: this.onPanelCornerDragEnd })
                    ]
                    :
                        [
                            React.createElement("div", { style: {
                                    // background: '#0000002e',
                                    position: 'absolute',
                                    top: 0,
                                    right: `-${this.dockboxPanelResizerOffset}px`,
                                    width: `${this.dockboxPanelResizerSize}px`,
                                    height: '100%'
                                }, onMouseEnter: this.handleMouseEnter('right'), onMouseLeave: this.handleMouseLeave }),
                            React.createElement("div", { style: {
                                    // background: '#0000002e',
                                    position: 'absolute',
                                    top: 0,
                                    left: `-${this.dockboxPanelResizerOffset}px`,
                                    width: `${this.dockboxPanelResizerSize}px`,
                                    height: '100%'
                                }, onMouseEnter: this.handleMouseEnter('left'), onMouseLeave: this.handleMouseLeave }),
                            React.createElement("div", { style: {
                                    // background: '#0000002e',
                                    position: 'absolute',
                                    top: `-${this.dockboxPanelResizerOffset}px`,
                                    left: 0,
                                    width: '100%',
                                    height: `${this.dockboxPanelResizerSize}px`
                                }, onMouseEnter: this.handleMouseEnter('top'), onMouseLeave: this.handleMouseLeave }),
                            React.createElement("div", { style: {
                                    // background: '#0000002e',
                                    position: 'absolute',
                                    bottom: `-${this.dockboxPanelResizerOffset}px`,
                                    left: 0,
                                    width: '100%',
                                    height: `${this.dockboxPanelResizerSize}px`
                                }, onMouseEnter: this.handleMouseEnter('bottom'), onMouseLeave: this.handleMouseLeave }),
                        ]),
            droppingLayer));
    }
    componentWillUnmount() {
        if (DockPanel._droppingPanel === this) {
            DockPanel.droppingPanel = null;
        }
        this._unmounted = true;
    }
}
DockPanel.contextType = DockContextType;
