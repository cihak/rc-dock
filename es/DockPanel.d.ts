import React from "react";
import { DockContext, PanelData } from "./DockData";
import { DragState } from "./dragdrop/DragManager";
interface Props {
    panelData: PanelData;
    size: number;
    canDockedPanelsBeMovedToFront: boolean;
}
interface State {
    dropFromPanel: PanelData;
    draggingHeader: boolean;
}
export declare class DockPanel extends React.PureComponent<Props, State> {
    static contextType: any;
    context: DockContext;
    _ref: HTMLDivElement;
    getRef: (r: HTMLDivElement) => void;
    static _droppingPanel: DockPanel;
    static set droppingPanel(panel: DockPanel);
    state: State;
    onDragOver: (e: DragState) => void;
    onDragOverOtherPanel(): void;
    _movingX: number;
    _movingY: number;
    onPanelHeaderDragStart: (event: DragState) => void;
    onPanelHeaderDragMove: (e: DragState) => void;
    onPanelHeaderDragEnd: (e: DragState) => void;
    _movingW: number;
    _movingH: number;
    _movingCorner: string;
    onPanelCornerDragTL: (e: DragState) => void;
    onPanelCornerDragTR: (e: DragState) => void;
    onPanelCornerDragBL: (e: DragState) => void;
    onPanelCornerDragBR: (e: DragState) => void;
    onPanelCornerDrag(e: DragState, corner: string): void;
    onPanelCornerDragMove: (e: DragState) => void;
    onPanelCornerDragEnd: (e: DragState) => void;
    onFloatPointerDown: () => void;
    onPanelClicked: () => void;
    _resizer: any;
    dockboxPanelResizerSize: number;
    dockboxPanelResizerOffset: number;
    topOffsetToSubtract: number;
    handleMouseEnter: (direction: any) => (e: any) => void;
    handleMouseLeave: (e: any) => void;
    render(): React.ReactNode;
    _unmounted: boolean;
    componentWillUnmount(): void;
}
export {};
