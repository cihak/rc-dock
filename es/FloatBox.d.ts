import React from "react";
import { BoxData } from "./DockData";
interface Props {
    boxData: BoxData;
    canDockedPanelsBeMovedToFront: boolean;
}
export declare class FloatBox extends React.PureComponent<Props, any> {
    render(): React.ReactNode;
}
export {};
