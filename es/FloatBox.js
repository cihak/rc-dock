import React from "react";
import { DockPanel } from "./DockPanel";
export class FloatBox extends React.PureComponent {
    render() {
        let { children } = this.props.boxData;
        const canDockedPanelsBeMovedToFront = this.props.canDockedPanelsBeMovedToFront;
        let childrenRender = [];
        for (let child of children) {
            if ('tabs' in child) {
                childrenRender.push(React.createElement(DockPanel, { size: child.size, panelData: child, canDockedPanelsBeMovedToFront: canDockedPanelsBeMovedToFront, key: child.id }));
            }
        }
        return (React.createElement("div", { className: 'dock-box dock-fbox' }, childrenRender));
    }
}
